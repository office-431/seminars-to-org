#!/bin/bash 
set -euo pipefail

# Configure the following variables:
# ORG_DIR: The directory in which to put the .org calendars
ORG_DIR="$HOME/org/agenda/"
# TMP_DIR: A temporary directory in which the .ics files are downloaded
TMP_DIR="/tmp"
# AUTHOR: Your name
AUTHOR="Your Name"
# EMAIL: Your email
EMAIL="your.name@fai.com"
# FILETAGS: Tags included in the org mode file.
FILETAGS="Work"

# These variables should be correct, and point to files included in this repository:
# CONFIG_FILE: Path to the file containing the different calendars to download
CONFIG_FILE="./calendars.txt"

# AWK_SCRIPT: Path to the ical2org AWK script
AWK_SCRIPT="./ical2org"

grep -v '^#\|^$' "$CONFIG_FILE" | while read -r line; do
    URL=$(echo "$line" | cut -d" " -f1)
    FILE=$(echo "$line" | cut -d" " -f2)
    CAT=$(echo "$line" | cut -d" " -f3)
    ICS_PATH="$TMP_DIR/$FILE"
    ORG_FILE="${FILE%.*}.org"
    ORG_PATH="$ORG_DIR/$ORG_FILE"
    curl "$URL" -o "$ICS_PATH"
    CALENDAR="$CAT" AUTHOR="$AUTHOR" EMAIL="$EMAIL" FILETAGS="$FILETAGS" \
	    "$AWK_SCRIPT" "$ICS_PATH" > "$ORG_PATH"
done
