# Seminars to Org

Script to download the schedule of different types of seminars of the LIG Laboratory and convert them to org-mode agenda files. Nothing innovative, uses wget and the ical2org awk script; more of a convenience script than anything else. 

## Configuration
In `download_calendars.sh`, edit the following variables:

- `ORG_DIR`: the directory in which the .org calendar files are to be put
- `TMP_DIR` (optionally): the temporary directory in which the .ics files are downloaded
- `AUTHOR`: Your name
- `EMAIL`: Your email
- `FILETAGS`: The tags to add in the .org file

In `calendars.txt`, comment out (and possibly add) ICS calendar sources. The format is the following:

```text
<url> <ics_file_name> <category>
```

The .ics files will be transformed into .org files of the same base name.
